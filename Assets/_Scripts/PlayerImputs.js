﻿#pragma strict

function Up (n : int)
{
	if (n == 1)
		return "Up";
		
	else if (n == 2)
		return "P2 Up";
}

function Down (n : int)
{
	if (n == 1)
		return "Down";
		
	else if (n == 2)
		return "P2 Down";
}

function Left (n : int)
{
	if (n == 1)
		return "Left";
		
	else if (n == 2)
		return "P2 Left";
}

function Right (n : int)
{
	if (n == 1)
		return "Right";
		
	else if (n == 2)
		return "P2 Right";
}

function Jump (n : int)
{
	if (n == 1)
		return "Jump";
		
	else if (n == 2)
		return "P2 Jump";
}

function Punch (n : int)
{
	if (n == 1)
		return "Punch";
		
	else if (n == 2)
		return "P2 Punch";
}