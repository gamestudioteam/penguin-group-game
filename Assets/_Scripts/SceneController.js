﻿#pragma strict

static var EnemiesEliminated01 = 0;

var b1Active = true;
var b2Active = true;
var b3Active = true;
var b4Active = true;
var b5Active = true;

private var players : GameObject[];

function Start()
{
	players = GameObject.FindGameObjectsWithTag("Player");
	EnemiesEliminated01 = 0;
}

function Update () {
	if(EnemiesEliminated01 == 2 && b1Active){
		GameObject.FindGameObjectWithTag("Barrier1").SetActive(false);
		b1Active = false;
		CameraFollow.CameraIsFollowing = 1;
		}
	if(EnemiesEliminated01 == 6 && b2Active){
		GameObject.FindGameObjectWithTag("Barrier2").SetActive(false);
		CameraFollow.CameraIsFollowing = 1;
		b2Active = false;
		}

	if(EnemiesEliminated01 == 9 && b3Active){
		GameObject.FindGameObjectWithTag("Barrier3").SetActive(false);
		CameraFollow.CameraIsFollowing = 1;
		b3Active = false;
		}
	if(EnemiesEliminated01 == 13 && b4Active){
		GameObject.FindGameObjectWithTag("Barrier4").SetActive(false);
		CameraFollow.CameraIsFollowing = 1;
		b4Active = false;
		}

	if(EnemiesEliminated01 == 14 && b5Active){
		GameObject.FindGameObjectWithTag("Barrier5").SetActive(false);
		CameraFollow.CameraIsFollowing = 1;
		b5Active = false;
		}
	if (Input.GetKeyDown(KeyCode.Y)){
			Debug.Log("You killed an enemy!");
			EnemiesEliminated01 = EnemiesEliminated01 + 1;
	}
	
	if(players[0].gameObject.GetComponent(PlayerController).GetIsDead() && players[1].gameObject.GetComponent(PlayerController).GetIsDead())
	{
		Invoke("endGame", 3.0);
		
	}
}

function endGame(){
	Application.LoadLevel(4);
}