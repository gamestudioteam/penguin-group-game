﻿#pragma strict

private var direction : String;

function OnTriggerEnter (other : Collider) 
{
	
	if(other.tag == "Enemy")
	{
		other.gameObject.GetComponent(EnemyController).GotHit(direction);
	}
			
}

function SetDirection(d : String)
{

	direction = d;

}