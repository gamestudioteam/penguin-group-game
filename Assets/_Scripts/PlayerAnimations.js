﻿#pragma strict

function Walk (n : int)
{
	if (n == 1)
		return "PC1Walk";
		
	else if (n == 2)
		return "PC2Walk";
}

function Jump (n : int)
{
	if (n == 1)
		return "PC1Jump";
		
	else if (n == 2)
		return "PC2Jump";
}

function Idle (n : int)
{
	if (n == 1)
		return "PC1Idle";
		
	else if (n == 2)
		return "PC2Idle";
}

function Punch (n : int)
{
	if (n == 1)
		return "PC1Punch";
		
	else if (n == 2)
		return "PC2Punch";
}

function Slide (n : int)
{
	if (n == 1)
		return "PC1Slide2";
		
	else if (n == 2)
		return "PC2Slide2";
}

function Reaction (n : int)
{
	if (n == 1)
		return "PC1Reaction";
		
	else if (n == 2)
		return "PC2Reaction";
}

function Dead (n : int)
{
	if (n == 1)
		return "PC1Dead";
		
	else if (n == 2)
		return "PC2Dead";
}