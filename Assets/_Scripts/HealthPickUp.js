﻿#pragma strict

var sound : AudioClip;

function OnTriggerEnter (other : Collider) {
	
	if(other.tag == "Player")
	{
			other.gameObject.GetComponent(PlayerController).SetHpToMax();
			Destroy(this.gameObject);
			AudioSource.PlayClipAtPoint(sound, transform.position);

	}


}