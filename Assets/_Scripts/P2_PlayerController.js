 #pragma strict

var nextSoundTime : float = 0; // can't play sounds if before this time
var maxHp : int = 10;
var speed : float = 10;
var jumpSpeed : float = 12;
var jumpReach : float = 4;
var slideSpeed : float = 10;
var slideDuration : float = 1.5;
var attackHitbox : GameObject;
var slideHitbox : GameObject;
var playerSprite : GameObject;
var hp : int;


private var rb : Rigidbody;
private var isJumping : boolean;
private var isPunching : boolean;
private var isSliding : boolean;
private var combo : int;
private var direction : String;
private var jumpPos : Vector3;
private var jz : float;
private var jx : float;
private var isDead : boolean;

public var HitReactionSound : AudioClip;
public var MoveSound : AudioClip;
public var JumpSound : AudioClip;
public var PunchSound : AudioClip;
private var source : AudioSource;
public var anim : Animator;

function Start () 
{
	source = GetComponent.<AudioSource>();
	rb = GetComponent.<Rigidbody>();
	isJumping = false;
	isPunching = false;
	isSliding = false;
	attackHitbox.SetActive(false);
	slideHitbox.SetActive(false);
	direction = "right";
	combo = 0;
	jumpPos = this.transform.position;
	hp = maxHp;
	
}


function FixedUpdate () 
{
		IsDead();
		
		rb.angularVelocity = Vector3.zero;
		SetDirection();
		
		//MOVE UP
		if (Input.GetButton("P2 Up") && isPunching == false && isSliding == false) 

		{
			MoveUp();
		}
		
		//MOVE DOWN				
		if (Input.GetButton("P2 Down") && isPunching == false && isSliding == false)
		{
			MoveDown();	
		}
		
		//MOVE LEFT
		if (Input.GetButton("P2 Left") && isPunching == false && isSliding == false)
		{
			MoveLeft();
		}
			
		//MOVE RIGHT	
		if (Input.GetButton("P2 Right") && isPunching == false && isSliding == false)
		{
			MoveRight();

		}
		
		//JUMP	
		if (Input.GetButton("P2 Jump") && isJumping == false && isPunching == false && isSliding == false)
		{
			Jump();			
		}
		
		//
		if (Input.GetButtonDown("P2 Punch"))
		{
			if (isJumping && isSliding == false)
			{
				SlideStart();
			}
			else if (isJumping == false && isSliding == false)
			{
				Attack(0.05);
			}
		}
		
}


function MoveUp()
{
	if (isJumping)
	{
		jz = this.transform.position.z - jumpPos.z;
		if(jz<0) {jz = (-1) * jz;}

		this.transform.Translate(Vector3.forward * (speed/2) * Time.deltaTime);
					
		if( jz > (jumpReach/2 ))
		{
			rb.velocity = Vector3.down * (jumpSpeed/2);
		}
	}
	else
	{
		this.transform.Translate(Vector3.forward * speed * Time.deltaTime);
		anim.Play("walk");
		playSteps();
	}
}

function MoveDown()
{
	if (isJumping)
	{
		jz = this.transform.position.z - jumpPos.z;
		if(jz<0) {jz = (-1) * jz;}
				
		this.transform.Translate(Vector3.back * (speed/2) * Time.deltaTime);

		if( jz > (jumpReach/2 ))
		{
			rb.velocity = Vector3.down * (jumpSpeed/2);
		}

	}
	else
	{
				this.transform.Translate(Vector3.back * speed * Time.deltaTime);
				anim.Play("walk");
				playSteps();
	}
}

function MoveLeft()
{
	direction = "left";
	if (isJumping)
	{
		jx = this.transform.position.x - jumpPos.x;
		if(jx<0) {jx = (-1) * jx;}
				
		this.transform.Translate(Vector3.left * (speed/2) * Time.deltaTime);
					
		if( jx > (jumpReach/2 ))
		{
				rb.velocity = Vector3.down * (jumpSpeed/2);
		}
	}
	else
	{
		this.transform.Translate(Vector3.left * speed * Time.deltaTime);
		anim.Play("walk");
		playSteps();
	}
}

function MoveRight()
{
	direction = "right";
	if (isJumping)
	{
		jx = this.transform.position.x - jumpPos.x;
		if(jx<0) {jx = (-1) * jx;}
				

		this.transform.Translate(Vector3.right * (speed/2) * Time.deltaTime);
					
		if( jx > (jumpReach/2 ))
		{
			rb.velocity = Vector3.down * (jumpSpeed/2);
		}
			
	}
	else
	{
		this.transform.Translate(Vector3.right * speed * Time.deltaTime);
		anim.Play("walk");
		playSteps();
	}
}

function Jump()
{
		anim.Play("PCJump");
		jumpPos = this.transform.position;
		isJumping = true;
		rb.velocity = Vector3.up * jumpSpeed;
		source.PlayOneShot(JumpSound, 1);
	
}


function OnCollisionEnter(other : Collision)
{
	rb.velocity = Vector3.zero;
	if(isJumping == true && (other.gameObject.tag == "Floor"))
	{
		Debug.Log("collided at something");
		isJumping = false;
		anim.Play("PCIdle");
	}
	if(isSliding == true && (other.gameObject.tag == "Floor" || other.gameObject.tag == "Enemy"))
	{
		SlideEnd();
	}
}

function Attack(time : float)
{
	isPunching = true;
	anim.Play("PCPunch");
	source.PlayOneShot(PunchSound, 1);
	attackHitbox.SetActive(true);
	attackHitbox.GetComponent(P1_PlayerAttackScript).SetDirection(this.GetDirection());
	yield WaitForSeconds(time);
	attackHitbox.SetActive(false);
	yield WaitForSeconds(0.5);
	isPunching = false;
}

function SlideStart()
{
	Debug.Log("sliding");
	isSliding = true;
	isJumping = false;
	anim.Play("PCSlide2");
	this.transform.localScale = Vector3(2,1,1);
	playerSprite.transform.localScale = Vector3(1.5,3,3);
	slideHitbox.SetActive(true);
	
	if(direction == "right")
	{
		rb.velocity = (Vector3.right * slideSpeed * Time.deltaTime) + (Vector3.down * slideSpeed * Time.deltaTime);
	}
	else if(direction == "left")
	{
		rb.velocity = (Vector3.left * slideSpeed * Time.deltaTime)  + (Vector3.down * slideSpeed * Time.deltaTime);
	}
}

function SlideEnd()
{
	
	if(direction == "right")
	{
		rb.velocity = Vector3.right * slideSpeed * Time.deltaTime;
	}
	else if(direction == "left")
	{
		rb.velocity = Vector3.left * slideSpeed * Time.deltaTime;
	}
	yield WaitForSeconds(slideDuration);
	slideHitbox.SetActive(false);
	this.transform.localScale = Vector3(1,2,1);
	playerSprite.transform.localScale = Vector3(3,1.5,3);
	anim.Play("PCIdle");
	isSliding = false;
}

function SetDirection()
{
	Debug.Log(direction);
	if(direction == "right")
	{
		attackHitbox.transform.position = this.transform.position + Vector3(0.5, 0.06, 0);
		playerSprite.transform.eulerAngles = Vector3(0, 180, 0);
	}
		
	else if(direction == "left")
	{
		attackHitbox.transform.position = this.transform.position + Vector3(-0.5, 0.06, 0);
		playerSprite.transform.eulerAngles = Vector3(0, 0, 0);
	}
}

function GetDirection()
{
	return direction;
}

function IsDead()
{
	if(hp <= 0)
	{
		//AudioSource.PlayClipAtPoint(DeathSound, transform.position);
		anim.Play("PCDead");
		isDead = true;
	}
}

function GotHit(d : String) 
{
	if(!isDead)
	{
		hp--;
		Debug.Log("Player Hit, hp:" + hp);
		
		if(d == "right")
			rb.AddForce(Vector3.right * 200);
			
		else if (d == "left")
			rb.AddForce(Vector3.left * 200);
	}
		
}


function SetHpToMax()
{
	hp = maxHp;
	Debug.Log("Player hp " + hp);
}

function playSteps(){
	
	// can't play if last one not finished:
	if(Time.time>=nextSoundTime) {
	source.PlayOneShot(MoveSound);
	// write down when we will be finished:
	nextSoundTime = Time.time + MoveSound.length;
	}
}

function GetIsDead()
{
	return isDead;
}
