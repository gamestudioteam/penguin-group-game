﻿#pragma strict

var nextSoundTime : float = 0; // can't play sounds if before this time
var maxHp : int = 10;
var speed : float = 10;
var jumpSpeed : float = 12;
var jumpReach : float = 4;
var slideSpeed : float = 500;
var slideDuration : float = 0.8;
var attackHitbox : GameObject;
var slideHitbox : GameObject;
var playerSprite : GameObject;

public var playerId : int;
public var hp : int;
private var rb : Rigidbody;
private var isJumping : boolean;
private var isPunching : boolean;
private var isSliding : boolean;
private var combo : int;
private var direction : String;
private var jumpPos : Vector3;
private var jz : float;
private var jx : float;
private var isDead : boolean;

public var DeathSound : AudioClip;
public var HitReactionSound : AudioClip;
public var MoveSound : AudioClip;
public var JumpSound : AudioClip;
public var PunchSound : AudioClip;
private var source : AudioSource;
public var anim : Animator;

function Start () 
{
	source = GetComponent.<AudioSource>();
	rb = GetComponent.<Rigidbody>();
	isJumping = false;
	isPunching = false;
	isSliding = false;
	attackHitbox.SetActive(false);
	slideHitbox.SetActive(false);
	direction = "right";
	combo = 0;
	jumpPos = this.transform.position;
	hp = maxHp;
	isDead = false;
	
}


function FixedUpdate () 
{
		if (!isDead)
		{
			IsDead();
		}
		
		//rb.angularVelocity = Vector3.zero;
		SetDirection();
		
		if(!isDead)
		{
			//MOVE UP
			if (Input.GetButton(this.gameObject.GetComponent(PlayerImputs).Up(playerId)) && isPunching == false && isSliding == false) 
			{
				Debug.Log("uuuu");
				MoveUp();
			}
			
			//MOVE DOWN				
			if (Input.GetButton(this.gameObject.GetComponent(PlayerImputs).Down(playerId)) && isPunching == false && isSliding == false)
			{
				Debug.Log("ddddd");
				MoveDown();	
			}
			
			//MOVE LEFT
			if (Input.GetButton(this.gameObject.GetComponent(PlayerImputs).Left(playerId)) && isPunching == false && isSliding == false)
			{
				Debug.Log("lllll");
				MoveLeft();
			}
				
			//MOVE RIGHT	
			if (Input.GetButton(this.gameObject.GetComponent(PlayerImputs).Right(playerId)) && isPunching == false && isSliding == false)
			{
				Debug.Log("rrrrr");
				MoveRight();
			}
			
			//JUMP	
			if (Input.GetButton(this.gameObject.GetComponent(PlayerImputs).Jump(playerId)) && isJumping == false && isPunching == false && isSliding == false)
			{
				Debug.Log("jjjjj");
				Jump();			
			}
			
			//
			if (Input.GetButtonDown(this.gameObject.GetComponent(PlayerImputs).Punch(playerId)))
			{
				Debug.Log("ppppp");
				if (isJumping && isSliding == false)
				{
					SlideStart();
				}
				else if (isJumping == false && isSliding == false)
				{
					Attack(0.05);
				}
			}
		}
}

function MoveUp()
{
	//this.transform.Translate(Vector3.forward * 20.0 * Time.deltaTime);
	if (isJumping)
	{
		jz = this.transform.position.z - jumpPos.z;
		if(jz<0) {jz = (-1) * jz;}

		this.transform.Translate(Vector3.forward * (speed/2) * Time.deltaTime);
					
		if( jz > (jumpReach/2 ))
		{
			rb.velocity = Vector3.down * (jumpSpeed/2);
		}
	}
	else
	{
		this.transform.Translate(Vector3.forward * speed * Time.deltaTime);
		anim.Play(this.gameObject.GetComponent(PlayerAnimations).Walk(playerId));
		playSteps();
	}
}

function MoveDown()
{
	if (isJumping)
	{
		jz = this.transform.position.z - jumpPos.z;
		if(jz<0) {jz = (-1) * jz;}
				
		this.transform.Translate(Vector3.back * (speed/2) * Time.deltaTime);

		if( jz > (jumpReach/2 ))
		{
			rb.velocity = Vector3.down * (jumpSpeed/2);
		}

	}
	else
	{
				this.transform.Translate(Vector3.back * speed * Time.deltaTime);
				anim.Play(this.gameObject.GetComponent(PlayerAnimations).Walk(playerId));
				playSteps();
	}
}

function MoveLeft()
{
	direction = "left";
	if (isJumping)
	{
		jx = this.transform.position.x - jumpPos.x;
		if(jx<0) {jx = (-1) * jx;}
				
		this.transform.Translate(Vector3.left * (speed/2) * Time.deltaTime);
					
		if( jx > (jumpReach/2 ))
		{
				rb.velocity = Vector3.down * (jumpSpeed/2);
		}
	}
	else
	{
		this.transform.Translate(Vector3.left * speed * Time.deltaTime);
		anim.Play(this.gameObject.GetComponent(PlayerAnimations).Walk(playerId));
		playSteps();
	}
}

function MoveRight()
{
	direction = "right";
	if (isJumping)
	{
		jx = this.transform.position.x - jumpPos.x;
		if(jx<0) {jx = (-1) * jx;}
				

		this.transform.Translate(Vector3.right * (speed/2) * Time.deltaTime);
					
		if( jx > (jumpReach/2 ))
		{
			rb.velocity = Vector3.down * (jumpSpeed/2);
		}
			
	}
	else
	{
		this.transform.Translate(Vector3.right * speed * Time.deltaTime);
		anim.Play(this.gameObject.GetComponent(PlayerAnimations).Walk(playerId));
		playSteps();
	}
}

function Jump()
{
		anim.Play(this.gameObject.GetComponent(PlayerAnimations).Jump(playerId));
		jumpPos = this.transform.position;
		isJumping = true;
		rb.velocity = Vector3.up * jumpSpeed;
		source.PlayOneShot(JumpSound, 1);
	
}


function OnCollisionEnter(other : Collision)
{
	rb.velocity = Vector3.zero;
	if(isJumping == true)
	{
		Debug.Log("collided at something");
		isJumping = false;
		anim.Play(this.gameObject.GetComponent(PlayerAnimations).Idle(playerId));
	}
	if(isSliding == true && (other.gameObject.tag == "Floor" || other.gameObject.tag == "Enemy"))
	{
		SlideEnd();
	}
}

function Attack(time : float)
{
	isPunching = true;
	anim.Play(this.gameObject.GetComponent(PlayerAnimations).Punch(playerId));
	source.PlayOneShot(PunchSound, 1);
	attackHitbox.SetActive(true);
	attackHitbox.GetComponent(P1_PlayerAttackScript).SetDirection(this.GetDirection());
	yield WaitForSeconds(time);
	attackHitbox.SetActive(false);
	yield WaitForSeconds(0.5);
	isPunching = false;
}

function SlideStart()
{
	Debug.Log("sliding");
	isSliding = true;
	isJumping = false;
	anim.Play(this.gameObject.GetComponent(PlayerAnimations).Slide(playerId));
	this.transform.localScale = Vector3(2,1,1);
	playerSprite.transform.localScale = Vector3(1.5,3,3);
	slideHitbox.SetActive(true);
	
	if(direction == "right")
	{
		rb.velocity = (Vector3.right * slideSpeed * Time.deltaTime) + (Vector3.down * slideSpeed * Time.deltaTime);
	}
	else if(direction == "left")
	{
		rb.velocity = (Vector3.left * slideSpeed * Time.deltaTime)  + (Vector3.down * slideSpeed * Time.deltaTime);
	}
}

function SlideEnd()
{
	//anim.Play(this.gameObject.GetComponent(PlayerAnimations).Slide(playerId));
	if(direction == "right")
	{
		rb.velocity = Vector3.right * slideSpeed * Time.deltaTime;
	}
	else if(direction == "left")
	{
		rb.velocity = Vector3.left * slideSpeed * Time.deltaTime;
	}
	yield WaitForSeconds(slideDuration);
	slideHitbox.SetActive(false);
	this.transform.localScale = Vector3(1,2,1);
	playerSprite.transform.localScale = Vector3(3,1.5,3);
	anim.Play(this.gameObject.GetComponent(PlayerAnimations).Idle(playerId));
	isSliding = false;
}

function SetDirection()
{
	Debug.Log(direction);
	if(direction == "right")
	{
		attackHitbox.transform.position = this.transform.position + Vector3(0.5, 0.06, 0);
		playerSprite.transform.eulerAngles = Vector3(-30, 180, 0);
	}
		
	else if(direction == "left")
	{
		attackHitbox.transform.position = this.transform.position + Vector3(-0.5, 0.06, 0);
		playerSprite.transform.eulerAngles = Vector3(30, 0, 0);
	}
}

function GetDirection()
{
	return direction;
}

function IsDead()
{
	if(hp <= 0)
	{
		AudioSource.PlayClipAtPoint(DeathSound, transform.position);
		anim.Play(this.gameObject.GetComponent(PlayerAnimations).Dead(playerId));
		isDead = true;
	}
}

function GotHit(d : String) 
{
	if(!isDead)
	{
		hp--;
		Debug.Log("Player Hit, hp:" + hp);
		anim.Play(this.gameObject.GetComponent(PlayerAnimations).Reaction(playerId));
		AudioSource.PlayClipAtPoint(DeathSound, transform.position);
		
		if(d == "right")
			rb.AddForce(Vector3.right * 200);
			
		else if (d == "left")
			rb.AddForce(Vector3.left * 200);
	}
		
}


function SetHpToMax()
{
	hp = maxHp;
	Debug.Log("Player hp " + hp);
}

function playSteps(){
	
	// can't play if last one not finished:
	if(Time.time>=nextSoundTime) {
	source.PlayOneShot(MoveSound);
	// write down when we will be finished:
	nextSoundTime = Time.time + MoveSound.length;
	}
}

function GetIsDead()
{
	return isDead;
}

function WhichPlayer()
{
	return playerId;
}