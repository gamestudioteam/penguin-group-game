﻿#pragma strict

private var rb : Rigidbody;
var isWalking : boolean;
var isIdle : boolean;
var maxHp : int;
var proximity : float = 7.5;

private var currentTarget : GameObject;
private var speed : float = 4;
private var direction : String ;
private var rnd : float ;
private var isPunching : boolean;
private var hp : int;
private var isDead : boolean;
private var beingHit : boolean;

private var players : GameObject[];

var attackHitbox : GameObject;
var canAttackHitbox : GameObject;

public var GotHitSound : AudioClip;
public var MoveSound : AudioClip;
public var JumpSound : AudioClip;
public var DeathSound : AudioClip;
public var anim : Animator;
public var enemySprite : GameObject;

private var source : AudioSource;

function Start () 
{
	source = GetComponent.<AudioSource>();
	rb = GetComponent.<Rigidbody>();
	isWalking = false;
	attackHitbox.SetActive(false);
	isPunching = false;
	hp = maxHp;
	isDead = false;
	beingHit = false;
	
	players = GameObject.FindGameObjectsWithTag("Player");
	
}

function FixedUpdate () {

	//IsDead();

	SetCurrentTarget();
	SetDirection();
	
	if (!isDead)
	{
		IsDead();
	}
	
	if(!isDead && !beingHit)
	{
		if(ProximityCheck())
		{
			if(CanAttack() && !isPunching)
			{
				Attack(0.05);
			}
			else if (isIdle && !isPunching)
			{
				isIdle = false;
				Walk();
			}
			
			if(isWalking && !isPunching)
			{
				
				var deltaX = this.transform.position.x - currentTarget.transform.position.x;
				
				//Debug.Log(deltaX);
				
				if(deltaX > 0)
				{
					direction = "left";
					//Debug.Log(direction);
					//Debug.Log(deltaX);
				}
				
				else
				{
					direction = "right";
					//Debug.Log(direction);
					//Debug.Log(deltaX);
				}
				
				//Debug.Log(deltaX);
				//Debug.Log(direction);
				
				
				this.transform.position = Vector3.MoveTowards(this.transform.position, currentTarget.transform.position, speed * Time.deltaTime);
			}
		}
	}
	
}

function SetCurrentTarget()
{
	//Debug.Log(players[1].transform.position);
	
	var p0dead = players[0].gameObject.GetComponent(PlayerController).GetIsDead();
	var p1dead = players[1].gameObject.GetComponent(PlayerController).GetIsDead();

	if(!p0dead && !p1dead)
	{
	
		currentTarget = players[0];
		
		for (var p in players) {
			
			if(Vector3.Distance(this.transform.position, currentTarget.transform.position) > Vector3.Distance(this.transform.position, p.transform.position) )
			{
				currentTarget = p;
			}
			
		}

	}

	else if(!p0dead && p1dead)
	{
		currentTarget = players[0];
	}
	
		else if(p0dead && !p1dead)
	{
		currentTarget = players[1];	
	}
}

function Walk()
{
	isWalking = true;
	anim.Play("walk");
	
	rnd = Random.Range(3,7);
	yield WaitForSeconds(rnd);
	isWalking = false;
	enemyWait();

}

function CanAttack()
{
	return canAttackHitbox.GetComponent.<EnemyCanAttack>().CanAttack();
}

function enemyWait()
{
	yield WaitForSeconds(2);
	isIdle = true;
}

function Attack(time : float)
{
	isPunching = true;
	anim.Play("punch");
	attackHitbox.SetActive(true);
	attackHitbox.GetComponent(EnemyAttackScript).SetDirection(this.GetDirection());
	yield WaitForSeconds(time);
	attackHitbox.SetActive(false);
	yield WaitForSeconds(0.5);
	isPunching = false;
}

function SetDirection()
{
	if(direction == "right")
	{
		attackHitbox.transform.position = this.transform.position + Vector3(0.5, 0.06, 0);
		canAttackHitbox.transform.position = this.transform.position + Vector3(0.5, 0.06, 0);
		enemySprite.transform.eulerAngles = Vector3(-30, 180, 0);
		
	}
		
	else if(direction == "left")
	{
		attackHitbox.transform.position = this.transform.position + Vector3(-0.5, 0.06, 0);
		canAttackHitbox.transform.position = this.transform.position + Vector3(-0.5, 0.06, 0);
		enemySprite.transform.eulerAngles = Vector3(30, 0, 0);
	}
}

function GetDirection()
{
	return direction;
}


function IsDead()
{
	if(hp <= 0)
	{
		AudioSource.PlayClipAtPoint(DeathSound, transform.position);
		anim.Play("death NotWorking");
		isDead = true;
		SceneController.EnemiesEliminated01 = SceneController.EnemiesEliminated01 + 1;
	}
}

function GotHit(d : String) 
{
	if(!isDead)
	{
		hp--;
		Debug.Log("Enemy Hit, hp:" + hp);
		source.PlayOneShot(GotHitSound, 1);
		beingHit = true;
		Invoke("EndGotHit",1);
		anim.Play("reaction1");
		
		if(d == "right")
			rb.AddForce(Vector3.right * 200);
			
		else if (d == "left")
			rb.AddForce(Vector3.left * 200);
	}
}

function EndGotHit() 
{
	beingHit = false;
}

function ProximityCheck ()
{
	var d = Vector3.Distance(this.transform.position, currentTarget.transform.position);
	//Debug.Log(d);
	
	if(d < proximity)
	{
		return true;
	}
	else
	{
		return false;
	}
}

