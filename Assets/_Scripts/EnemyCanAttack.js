﻿#pragma strict

private var canAttack : boolean;

function Start () {
	
	canAttack = false;
	
}

function Update () {

}

function OnTriggerStay (other : Collider) {
	
	if(other.gameObject.tag == "Player")
	{
		canAttack = true;
	}
	
}

function OnTriggerExit (other : Collider) {
	
	if(other.gameObject.tag == "Player")
	{
		canAttack = false;
	}
	
}

function CanAttack() {

	return canAttack;

}