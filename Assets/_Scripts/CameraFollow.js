﻿#pragma strict

public var speed : float = 0.1f;
var target : GameObject;
var zpos : float;
var xpos : float;
var ypos : float;
static var CameraIsFollowing = 0;

private var players : GameObject[];

function Start () 
{
	
	players = GameObject.FindGameObjectsWithTag("Player");
	
}

function Update()
{
	SetTarget();
	if(CameraIsFollowing == 1) {
		transform.position = Vector3.Lerp(transform.position + Vector3 (xpos, ypos, zpos), target.transform.position, speed);
		
		transform.position.z = zpos;
		transform.position.y = ypos;
		
		//transform.position = transform.position + Vector3 (xpos, ypos, zpos);
	}
}

function SetTarget()
{
	var p0dead = players[0].gameObject.GetComponent(PlayerController).GetIsDead();
	var p1dead = players[1].gameObject.GetComponent(PlayerController).GetIsDead();

	if(!p0dead && !p1dead)
	{
	
		target = players[0];		

	}

	else if(!p0dead && p1dead)
	{
		target = players[0];
	}
	
	else if(p0dead && !p1dead)
	{
		target = players[1];	
	}

}