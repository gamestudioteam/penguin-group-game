﻿#pragma strict

private var rb : Rigidbody;
var speed : float = 5;
var isWalking : boolean;
var isIdle : boolean;
private var currentTarget : GameObject;

function Start () 
{
	rb = GetComponent.<Rigidbody>();
	isWalking = false;
	
}

function FixedUpdate () {

	SetCurrentTarget();
	
	if(CanAttack() && !isIdle)
	{
		
	}
	else if (!isIdle)
	{
		Walk();
	}
	
	if(isWalking)
	{
		this.transform.position = Vector3.MoveTowards(this.transform.position, currentTarget.transform.position, speed * Time.deltaTime);
	}

}

function SetCurrentTarget()
{
	currentTarget = GameObject.FindWithTag("Player");
}

function Walk()
{
	isWalking = true;
	Debug.Log(isWalking);
	yield WaitForSeconds(3);
	isWalking = false;
	enemyWait();
	Debug.Log(isWalking);
}

function CanAttack()
{
	return false;
}

function enemyWait()
{
	isIdle = true;
	yield WaitForSeconds(0.5);
	isIdle = false;
}

function GotHit(d : String) 
{
	Debug.Log("I was hit");
	SceneController.EnemiesEliminated01 = 1;
	Destroy(this.gameObject);
	if(d == "right")
		rb.AddForce(Vector3.right * 200);

		
	else if (d == "left")
		rb.AddForce(Vector3.left * 200);
		
	/*
	if(P1_PlayerController.GetDirection() == "left")
	{
		Debug.Log("left");
	}
	else if(P1_PlayerController.GetDirection() == "right")
	{
		Debug.Log("right");
	}
	*/
}